package StepDefinations;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.CucumberOptions.SnippetType;
@CucumberOptions(
		features = {"src/test/resources"},
		glue = "StepDefinations",
		snippets = SnippetType.CAMELCASE,
		monochrome = true
		//dryRun = true
	   )
public class RunnerClasss extends AbstractTestNGCucumberTests {
}
